import 'package:flutter/material.dart';
import 'package:rectube/views/Card.dart';
import 'package:rectube/src/SavedVideos.dart';
import 'package:rectube/views/SideDrawer.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SavedVideoList extends StatefulWidget {
  @override
  _SavedVideoListState createState() => _SavedVideoListState();
}

class _SavedVideoListState extends State<SavedVideoList> {
  var savedVideos = new SavedVideos();

  @override
  void initState() {
    super.initState();
    savedVideos.getVideos().then((_) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Saved Video"),
      ),
      drawer: SideDrawer(),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        child: ListView.builder(
            itemCount: savedVideos.videos.length,
            itemBuilder: (context, int index) {
              return Slidable(
                delegate: SlidableDrawerDelegate(),
                child: ListCard(
                  title: savedVideos.videos[index]["title"],
                  url: savedVideos.videos[index]["url"],
                  image: savedVideos.videos[index]["image"],
                  views: 0,
                ),
                actions: <Widget>[
                  IconSlideAction(
                    icon: FontAwesomeIcons.trash,
                    color: Colors.red,
                    caption: "Delete",
                    onTap: () {
                      savedVideos
                          .removeVideo(savedVideos.videos[index])
                          .then((_) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text("Video removed from saved list")));
                        savedVideos.getVideos().then((_) {
                          setState(() {});
                        });
                      });
                    },
                  )
                ],
              );
            }),
      ),
    );
  }
}
