import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rectube/views/VideoList.dart';
import 'package:rectube/views/Player.dart';
import 'package:cached_network_image/cached_network_image.dart';

class TagCard extends StatelessWidget {
  final String content;
  TagCard({this.content});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 30.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          gradient: LinearGradient(colors: [Colors.black, Colors.black])),
      child: Hero(
        tag: content,
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => Content(
                      title: content,
                    )));
          },
          child: Center(
            child: Text(
              content.toUpperCase(),
              textScaleFactor: 1.2,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}

class ListCard extends StatelessWidget {
  final String id;
  final String image;
  final String title;
  final String url;
  final num views;

  ListCard({this.id, this.image, this.title, this.url, this.views});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: InkWell(
        onTap: () {
          updateViews().then((_) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => Player(
                      title: title,
                      url: url,
                    )));
          });
        },
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 3.0),
                  child: Container(
                    height: 80.0,
                    width: 100.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(9.0),
                        image: DecorationImage(
                          image: CachedNetworkImageProvider(image),
                        )),
                  ),
                ),
                SizedBox(
                  width: 20.0,
                ),
                Flexible(
                  child: Hero(
                      tag: title,
                      child: Text(
                        title,
                        textScaleFactor: 0.9,
                        style: TextStyle(color: Colors.black),
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> updateViews() async {
//    print(id);
//    Firestore db = Firestore.instance;
//
//    if (id != null) {
//      final TransactionHandler updateTransaction = (Transaction tx) async {
//        final DocumentSnapshot ds =
//            await tx.get(db.collection('videos').document(id));
//        tx.update(ds.reference, {'views': views + 1});
//      };
//
//      await Firestore.instance.runTransaction(updateTransaction);
//    }
  }
}

String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
