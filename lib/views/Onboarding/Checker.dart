import 'package:shared_preferences/shared_preferences.dart';

Future<bool> seen() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setBool('isSeen', true);
  return true;
}

Future<bool> isOld() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool('isSeen') ?? false;
}
