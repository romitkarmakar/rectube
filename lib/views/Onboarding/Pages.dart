import 'package:flutter/material.dart';

class PageModel {
  var imageUrl;
  var title;
  var body;
  List<Color> titleGradient = [];
  PageModel({this.imageUrl, this.title, this.body, this.titleGradient});
}

List<List<Color>> gradients = [
  [Color(0xFF9708CC), Color(0xFF43CBFF)],
  [Color(0xFFE2859F), Color(0xFFFCCF31)],
  [Color(0xFF5EFCE8), Color(0xFF736EFE)],
];

var pageList = [
  PageModel(
      imageUrl: "assets/1.svg",
      title: "Play Anywhere",
      body: "Now play anywhere on the go with saved playlists",
      titleGradient: gradients[0]),
  PageModel(
      imageUrl: "assets/2.svg",
      title: "Play the Best",
      body:
          "Now get the best videos according to your taste, no need to scroll through thousands of playlists.",
      titleGradient: gradients[1]),
  PageModel(
      imageUrl: "assets/3.svg",
      title: "Share with everyone",
      body:
          "Now share your fsvourite videos among your family and your friends and spread your love to them.",
      titleGradient: gradients[2]),
];
