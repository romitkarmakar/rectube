import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart' as yp;
import 'package:rectube/views/Components/YoutubePlayer.dart';
import 'package:youtube_extractor/youtube_extractor.dart';
import 'package:rectube/src/Downloader.dart';
import 'package:rectube/src/MusicPlayer.dart';

class Player extends StatefulWidget {
  final String url;
  final String title;

  Player({this.url, this.title});

  @override
  _PlayerState createState() => _PlayerState();
}

class _PlayerState extends State<Player> {
  yp.YoutubePlayerController _controller = yp.YoutubePlayerController();
  bool _muted = false;
  var downloader;
  var videoId;
  var extractor = YouTubeExtractor();
  var music = new MusicPlayer();

  Future<String> getVideoUrl() async {
    var video = widget.url.split("=")[1];
    var videoInfo = await extractor.getMediaStreamsAsync(video);

    return videoInfo.video.first.url;
  }

  Future<String> getAudioUrl() async {
    var video = widget.url.split("=")[1];
    var videoInfo = await extractor.getMediaStreamsAsync(video);
    return videoInfo.audio.first.url;
  }

  void listener() {
    if (_controller.value.playerState == yp.PlayerState.ENDED) {
      Navigator.of(context).pop();
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    downloader = new Downloader();
  }

  @override
  void dispose() {
    super.dispose();
    music.stop();
  }

  @override
  void deactivate() {
    _controller.pause();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Hero(tag: widget.title, child: Text("Watch Video")),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          YoutubePlayer(context, widget.url, _controller, listener),
          SizedBox(
            height: 20.0,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 2.0),
            child: Text(
              widget.title,
              textScaleFactor: 1.2,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(_muted ? Icons.volume_off : Icons.volume_up),
                onPressed: () {
                  _muted ? _controller.unMute() : _controller.mute();
                  setState(() {
                    _muted = !_muted;
                  });
                },
              ),
              Container(
                decoration:
                    BoxDecoration(color: Colors.red, shape: BoxShape.circle),
                child: IconButton(
                  iconSize: 60.0,
                  icon: Icon(
                    _controller.value.isPlaying
                        ? Icons.pause
                        : Icons.play_arrow,
                  ),
                  onPressed: () {
                    _controller.value.isPlaying
                        ? _controller.pause()
                        : _controller.play();
                    setState(() {});
                  },
                ),
              ),
              IconButton(
                  icon: Icon(Icons.file_download),
                  onPressed: () {
                    getVideoUrl().then((id) {
                      print("Video Id: $id");

                      downloader.addDownload(widget.title, id, "mp4");
                    });
                  }),
              IconButton(
                  icon: Icon(Icons.fullscreen),
                  onPressed: () => _controller.enterFullScreen()),
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                  icon: music.isPlaying()
                      ? Icon(Icons.pause_circle_filled)
                      : Icon(Icons.play_arrow),
                  onPressed: () {
                    if (music.isPlaying())
                      setState(() {
                        music.pause();
                      });
                    else
                      setState(() {
                        getAudioUrl().then((url) {
                          music.play(url);
                        });
                      });
                  }),
              IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    music.stop();
                  }),
            ],
          )
        ],
      ),
    );
  }
}
