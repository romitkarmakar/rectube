import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:rectube/controller/DrawerController.dart';
import 'package:rectube/views/SavedVideoList.dart';
import 'package:rectube/views/TagList.dart';
import 'package:rectube/views/YoutubeSite.dart';

class SideDrawer extends StatefulWidget {
  @override
  _SideDrawerState createState() => _SideDrawerState();
}

class _SideDrawerState extends State<SideDrawer> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    load().then((_) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(user.name),
            accountEmail: Text(user.email),
            currentAccountPicture: CachedNetworkImage(
              imageUrl: user.imgUrl,
              placeholder: (context, url) => new CircularProgressIndicator(),
              errorWidget: (context, url, error) => new Icon(Icons.error),
            ),
          ),
          ListTile(
            title: Text("Home"),
            leading: Icon(FontAwesomeIcons.home),
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => TagList()));
            },
          ),
          ListTile(
            title: Text("Saved Videos"),
            leading: Icon(FontAwesomeIcons.history),
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => SavedVideoList()));
            },
          ),
          ListTile(
            title: Text("Audio Player"),
            leading: Icon(FontAwesomeIcons.fileAudio),
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => YoutubeSite()));
            },
          )
        ],
      ),
    );
  }
}
