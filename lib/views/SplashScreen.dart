import 'package:flutter/material.dart';
import 'package:rectube/views/TagList.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rectube/views/Authentication.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void timeOut() async {
    var prefs = await SharedPreferences.getInstance();
    if (prefs.getString('email') != null) {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => TagList()));
    } else
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => Authentication()));
  }

  @override
  void initState() {
    super.initState();
    Timer(
        Duration(
          seconds: 5,
        ),
        timeOut);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 100.0,
              ),
              Image.asset(
                "assets/logo.png",
                height: 120.0,
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                "Get the best from Youtube",
                textScaleFactor: 1.5,
              ),
              SizedBox(
                height: 200.0,
              ),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.red),
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
