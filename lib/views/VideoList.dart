import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:rectube/views/Card.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:rectube/src/SavedVideos.dart';
import 'package:share/share.dart';

class Content extends StatefulWidget {
  final String title;
  Content({this.title});

  @override
  _ContentState createState() => _ContentState();
}

class _ContentState extends State<Content> {
  var videos = <Map>[];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    videos = [];
    Firestore.instance
        .collection('videos')
        .where("tag", isEqualTo: widget.title)
        .orderBy("views", descending: true)
        .snapshots()
        .listen((data) => data.documents.forEach((doc) {
              videos.add({
                'id': doc.documentID,
                'views': doc['views'],
                'title': doc["title"],
                'url': doc["url"],
                'image': doc["image"]
              });
              setState(() {});
            }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title:
              Hero(tag: widget.title, child: Text(widget.title.toUpperCase())),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: ListView.builder(
              itemCount: videos.length,
              itemBuilder: (context, int index) {
                return Slidable(
                    delegate: new SlidableDrawerDelegate(),
                    actions: <Widget>[
                      IconSlideAction(
                        caption: "Save Video",
                        icon: FontAwesomeIcons.save,
                        color: Colors.blue,
                        onTap: () {
                          var savedVideos = new SavedVideos();
                          savedVideos.addVideo(videos[index]).then((_) {
                            Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text("Video Saved")));
                          });
                        },
                      ),
                      IconSlideAction(
                        caption: "Share",
                        icon: FontAwesomeIcons.share,
                        color: Colors.red,
                        onTap: () {
                          Share.share(videos[index]['url']);
                        },
                      )
                    ],
                    child: ListCard(
                      id: videos[index]['id'],
                      views: videos[index]['views'],
                      title: videos[index]['title'],
                      url: videos[index]['url'],
                      image: videos[index]['image'],
                    ));
              }),
        ));
  }
}
