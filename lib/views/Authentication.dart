import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import 'package:rectube/views/TagList.dart';
import 'package:rectube/controller/SignInController.dart';
import 'Onboarding/Checker.dart';
import 'Onboarding/Onboarding.dart';

class Authentication extends StatefulWidget {
  @override
  _AuthenticationState createState() => _AuthenticationState();
}

class _AuthenticationState extends State<Authentication> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            Colors.blue,
            Colors.blueAccent,
            Colors.lightBlue,
            Colors.lightBlueAccent
          ])),
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Center(
            child: InkWell(
              child: SizedBox(
                width: 220.0,
                height: 50.0,
                child: RaisedButton(
                  color: Colors.red,
                  onPressed: () {
                    handleSignIn().then((_) {
                      isOld().then((flag) {
                        if (flag == true)
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => TagList()));
                        else
                          Navigator.of(context).pushReplacement(
                              new MaterialPageRoute(
                                  builder: (context) => Onboarding()));
                      });
                    });
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.google,
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        "Sign In with Google",
                        textScaleFactor: 1.2,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
