import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart' as yp;

Widget YoutubePlayer(context, url, controller, listener) {
  return yp.YoutubePlayer(
            context: context,
            aspectRatio:
                MediaQuery.of(context).orientation == Orientation.portrait
                    ? 16 / 9
                    : 16 / 7,
            videoId: yp.YoutubePlayer.convertUrlToId(url),
            autoPlay: false,
            hideControls: false,
            bufferIndicator: CircularProgressIndicator(),
            showVideoProgressIndicator: true,
            videoProgressIndicatorColor: Colors.red,
            progressColors: yp.ProgressColors(
              playedColor: Colors.red,
              handleColor: Colors.red,
              bufferedColor: Colors.green,
            ),
            onPlayerInitialized: (controller) {
              controller = controller;
              controller.addListener(listener);
            },
          );
}