import 'package:ads/ads.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rectube/views/SideDrawer.dart';
import 'package:rectube/views/Card.dart';

class TagList extends StatefulWidget {
  @override
  _TagListState createState() => _TagListState();
}

class _TagListState extends State<TagList> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Ads.init('ca-app-pub-1026159358388751/2209374253', testing: false);
    Ads.showBannerAd();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Recommendations'),
      ),
      drawer: SideDrawer(),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: StreamBuilder<QuerySnapshot>(
          stream: Firestore.instance.collection('tags').snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return new Center(child: CircularProgressIndicator());
              default:
                return new StaggeredGridView.countBuilder(
                  crossAxisCount: 4,
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (BuildContext context, int index) {
                    List<dynamic> value = snapshot.data.documents
                        .map((DocumentSnapshot document) {
                      return document['name'];
                    }).toList();

                    return TagCard(
                      content: value[index],
                    );
                  },
                  staggeredTileBuilder: (int index) =>
                      new StaggeredTile.count(2, index.isEven ? 2 : 1),
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                );
            }
          },
        ),
      ),
    );
  }
}