import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'dart:io';

class Downloader {
  List tasks = <Task>[];
  String localPath;

  Downloader() {
    FlutterDownloader.registerCallback((id, status, progress) {
      var element = tasks.firstWhere((task) => task.taskId == id);
      tasks[element["taskId"]].status = status;
      tasks[element["taskId"]].progress = progress;
    });

    prepare();
  }

  Future<void> prepare() async {
    await FlutterDownloader.loadTasks();
    localPath = (await findLocalPath()) + '/Download';

    final savedDir = Directory(localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }

  Future<void> addDownload(name, url, format) async {
    var task = new Task(name: name, url: url);

    task.taskId = await FlutterDownloader.enqueue(
        url: task.url,
        headers: {"auth": "test_for_sql_encoding"},
        savedDir: localPath,
        showNotification: true,
        fileName: "$name.$format",
        openFileFromNotification: true);

    tasks.add(task);
  }

  Future<String> findLocalPath() async {
    final directory = await getExternalStorageDirectory();
    return directory.path;
  }
}

class Task {
  String name;
  String url;
  String taskId;
  num progress = 0;
  DownloadTaskStatus status = DownloadTaskStatus.undefined;

  Task({this.name, this.url});
}
