import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SavedVideos {
  List<Map<dynamic, dynamic>> videos = [];
  String dbName = "recTube.db";
  String table = "videos";
  num length = 0;
  Database database;

  Future<void> initialize() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, dbName);

    database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      // When creating the db, create the table
      await db.execute(
          'CREATE TABLE $table (id INTEGER PRIMARY KEY, title TEXT, url TEXT, image TEXT)');
    });

    length = Sqflite.firstIntValue(
        await database.rawQuery('SELECT COUNT(*) FROM $table'));
  }

  Future<int> addVideo(Map video) async {
    await initialize();
    int id;

    await database.transaction((txn) async {
      id = await txn.rawInsert(
          'INSERT INTO $table (title, url, image) VALUES(?, ?, ?)',
          [video["title"], video["url"], video["image"]]);
    });

    return id;
  }

  Future<void> getVideos() async {
    await initialize();
    videos = await database.rawQuery('SELECT * FROM $table');
  }

  Future<void> removeVideo(Map video) async {
    await initialize();
    length = await database
        .rawDelete('DELETE FROM $table WHERE title = ?', [video["title"]]);
  }
}
