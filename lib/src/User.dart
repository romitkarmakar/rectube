import 'package:shared_preferences/shared_preferences.dart';

class User {
  String name = "loading...";
  String email = "loading...";
  String imgUrl = "loading...";

  Future<bool> fromLocal() async {
    var prefs = await SharedPreferences.getInstance();

    name = prefs.getString('name');
    email = prefs.getString('email');
    imgUrl = prefs.getString('imgUrl');

    return true;
  }

  Future<bool> persist() async {
    final prefs = await SharedPreferences.getInstance();

    prefs.setString('name', name);
    prefs.setString('email', email);
    prefs.setString('imgUrl', imgUrl);

    return true;
  }

  Future<bool> removeLocal() async {
    final prefs = await SharedPreferences.getInstance();

    prefs.remove('name');
    prefs.remove('email');
    prefs.remove('imgUrl');

    return true;
  }
}
