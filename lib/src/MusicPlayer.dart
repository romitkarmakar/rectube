import 'package:audioplayer/audioplayer.dart';

enum MusicState { stopped, playing, paused }

class MusicPlayer {
  AudioPlayer audioPlayer;
  var musicState = MusicState.stopped;

  MusicPlayer() {
    audioPlayer = new AudioPlayer();
  }

  Future play(url) async {
    await audioPlayer.play(url);
    musicState = MusicState.playing;
  }

  Future pause() async {
    await audioPlayer.pause();
    musicState = MusicState.paused;
  }

  Future stop() async {
    await audioPlayer.stop();
    musicState = MusicState.stopped;
  }

  bool isPlaying() {
    if (musicState == MusicState.playing) return true;
    return false;
  }
}
