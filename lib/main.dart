import 'package:flutter/material.dart';
import 'package:rectube/views/SplashScreen.dart';

void main() => runApp(RecTubeApp());

class RecTubeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'RecTube',
      home: SplashScreen(),
    );
  }
}
