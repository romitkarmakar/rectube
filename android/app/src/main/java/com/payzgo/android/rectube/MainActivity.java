package com.payzgo.android.rectube;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;

import io.flutter.app.FlutterActivity;

import io.flutter.plugin.common.MethodChannel;

import io.flutter.plugins.GeneratedPluginRegistrant;


public class MainActivity extends FlutterActivity {
  private static final String CHANNEL = "samples.flutter.io/battery";

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    Bundle extras = intent.getExtras();

    if(extras == null)
    {
      //Cry about not being clicked on
    }
    else if (extras.getInt("NotiClick") == 1)
    {
      new MethodChannel(getFlutterView(), CHANNEL).invokeMethod("message", "Playing");
    } else if (extras.getInt("NotiClick") == 2)
    {
      new MethodChannel(getFlutterView(), CHANNEL).invokeMethod("message", "Stopped");
    } else if (extras.getInt("NotiClick") == 0)
    {
      new MethodChannel(getFlutterView(), CHANNEL).invokeMethod("message", "Paused");
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);

    if (savedInstanceState == null) {
      Bundle extras = getIntent().getExtras();
      if(extras == null)
      {
        //Cry about not being clicked on
      }
      else if (extras.getInt("NotiClick") == 1)
      {
        new MethodChannel(getFlutterView(), CHANNEL).invokeMethod("message", "Playing");
      } else if (extras.getInt("NotiClick") == 2)
      {
        new MethodChannel(getFlutterView(), CHANNEL).invokeMethod("message", "Stopped");
      } else if (extras.getInt("NotiClick") == 0)
      {
        new MethodChannel(getFlutterView(), CHANNEL).invokeMethod("message", "Paused");
      }

    }



    new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler((call, result) -> {
      if (call.method.equals("getBatteryLevel")) {
        int batteryLevel = getBatteryLevel();

        if (batteryLevel != -1) {
          result.success(batteryLevel);
        } else {
          result.error("UNAVAILABLE", "Battery level not available.", null);
        }

      } else {
        result.notImplemented();
      }
    });
  }

  private int getBatteryLevel() {
    int batteryLevel = -1;
    if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
      BatteryManager batteryManager = (BatteryManager) getSystemService(BATTERY_SERVICE);
      batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
    } else {
      Intent intent = new ContextWrapper(getApplicationContext()).registerReceiver(null,
              new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
      batteryLevel = (intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100)
              / intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
    }

    showNotification();

    return batteryLevel;
  }

  public void showNotification() {
    createNotificationChannel();

    Intent intent = new Intent(this, MainActivity.class);
    intent.putExtra("NotiClick",true);
    PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    Intent playIntent = new Intent(this, MainActivity.class);
    playIntent.putExtra("NotiClick",1);
    PendingIntent pendingPlayIntent = PendingIntent.getActivity(this, 0, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    Intent pauseIntent = new Intent(this, MainActivity.class);
    pauseIntent.putExtra("NotiClick",0);
    PendingIntent pendingPauseIntent = PendingIntent.getActivity(this, 1, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    Intent stopIntent = new Intent(this, MainActivity.class);
    stopIntent.putExtra("NotiClick",2);
    PendingIntent pendingStopIntent = PendingIntent.getActivity(this, 2, stopIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    Notification Noti;
    Noti = new Notification.Builder(this)
            .setContentTitle("YourTitle")
            .setContentText("YourDescription")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pIntent)
            .setStyle(new Notification.MediaStyle())
            .addAction(R.drawable.ic_play_arrow_black_24dp, "PLAY", pendingPlayIntent)
            .addAction(R.drawable.ic_not_interested_black_24dp, "STOP", pendingStopIntent)
            .setAutoCancel(true).build();

    NotificationManager notificationManager =
            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    notificationManager.notify(0, Noti);

//    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
//    notificationManager.notify(52, builder.build());
  }

  private void createNotificationChannel() {
    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      CharSequence name = "Test notif";
      String description = "Hi i am romit";
      int importance = NotificationManager.IMPORTANCE_DEFAULT;
      NotificationChannel channel = new NotificationChannel(CHANNEL, name, importance);
      channel.setDescription(description);
      // Register the channel with the system; you can't change the importance
      // or other notification behaviors after this
      NotificationManager notificationManager = getSystemService(NotificationManager.class);
      notificationManager.createNotificationChannel(channel);
    }
  }
}

